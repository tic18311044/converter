package com.montano.oquita.convertidor;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.montano.oquita.convertidor.databinding.ActivityMainBinding;
//31/05/2021
public class MainActivity extends Activity {

    public double miles, kms;
    public String result, miles_string;


    TextView results_tv=(TextView)findViewById(R.id.tv_km);
    EditText miles_et=(EditText)findViewById(R.id.ed_miles);
    Button result_btn=(Button)findViewById(R.id.btn_convert);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConvertMiles();
            }
        });
        //setAmbientEnable();

    }
    public void ConvertMiles(){
        // 1 mi = 1.69km
        miles_string=miles_et.getEditableText().toString();
        miles=Double.parseDouble(miles_string);
        kms=miles*1.69;
        result=kms+" KM";
        results_tv.setText(result);
    }
}